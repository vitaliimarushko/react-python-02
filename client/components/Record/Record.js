import React from 'react';
import PropTypes from 'prop-types';

import styles from './Record.css';

class Record extends React.Component {
  constructor (props) {
    super(props);

    this.removeRecord = this.removeRecord.bind(this);
    this.changeStatus = this.changeStatus.bind(this);
  };

  removeRecord (id) {
    return () => {
      this.props.removeRecord(id);
    };
  };

  changeStatus (id) {
    return () => {
      this.props.changeStatus(id);
    };
  }

  render () {
    let classString = styles.recordLiSpan;
    let record = this.props.record;

    if (this.props.record.checked) {
      classString += ' ' + styles.strike;
    }

    return (
      <div>
        <li className={styles.recordItem}>
          <input
            type="checkbox"
            value={record._id}
            defaultChecked={!!this.props.record.checked}
            onClick={this.changeStatus(record._id)}
          />
          <span className={classString}>{record.value}</span>
          <button onClick={this.removeRecord(record._id)}>x</button>
        </li>
      </div>
    );
  }
}

Record.propTypes = {
  record: PropTypes.shape({
    _id: PropTypes.number.isRequired,
    value: PropTypes.string.isRequired,
    checked: PropTypes.bool.isRequired
  }).isRequired,
};

export default Record;