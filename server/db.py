import pymongo


class Db:
    db_connection = None
    db_name = "todos"
    records_table = "records"

    @staticmethod
    def get_connection(self):
        if self.db_connection is None:
            db_client = pymongo.MongoClient("localhost", 27017)
            self.db_connection = db_client[Db.db_name]

        return self.db_connection

    @staticmethod
    def get_records(self, id=None):
        collection = self.get_connection(self)[self.records_table]
        options = {}

        if id is not None:
            options = {"_id": id}

        return collection.find(options)

    @staticmethod
    def insert_record(self, data):
        collection = self.get_connection(self)[self.records_table]
        inserted_record = collection.insert_one(data)
        return inserted_record.inserted_id

    @staticmethod
    def update_record(self, data):
        collection = self.get_connection(self)[self.records_table]

        return collection.find_one_and_update({"_id": data['_id']},
                                              {"$set": {
                                                  "checked": data['checked']
                                              }})


    @staticmethod
    def remove_record(self, id):
        collection = self.get_connection(self)[self.records_table]
        deleted_record = collection.delete_many({"_id": id})
        return deleted_record.deleted_count
