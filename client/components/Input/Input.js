import React from 'react';

import styles from './Input.css';

class Input extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      inputValue: ''
    };

    this.addRecord = this.addRecord.bind(this);
    this.changeContent = this.changeContent.bind(this);
  };

  addRecord () {
    const value = this.state.inputValue.toString().trim();

    if (value) {
      this.props.addRecord(value);

      this.setState({
        inputValue: ''
      });
    }
  };

  changeContent (e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render () {
    return (
      <div className={styles.inputContainer}>
        <input
          className={styles.inputTodoContent}
          name="inputValue"
          value={this.state.inputValue}
          onChange={this.changeContent}
        />
        <button
          className={styles.addTodoButton}
          onClick={this.addRecord}
        >ADD
        </button>
      </div>
    );
  };
}

export default Input;
