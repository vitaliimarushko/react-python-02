import React from 'react';
import Input from './Input/Input';
import List from './List/List';
import axios from 'axios/index';

import styles from './App.css';

class App extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      lastId: 0,
      todoItems: []
    };

    this.addRecord = this.addRecord.bind(this);
    this.changeStatus = this.changeStatus.bind(this);
    this.removeRecord = this.removeRecord.bind(this);
  };

  componentWillMount () {
    const me = this;

    axios.get('/data')
      .then((response) => {
        const data = response.data;

        if (data.success) {
          const lastId = data.data.reduce((acc, item) => {
            if (item._id > acc) {
              acc = item._id;
            }

            return acc;
          }, 0);

          me.setState({
            lastId: lastId,
            todoItems: data.data
          });
        }
      })
      .catch((error) => {
        console.log(error);
        alert('Error occurred. See details in the developer console (F12).');
      });
  }

  addRecord (value) {
    const me = this;
    const record = {
      _id: ++me.state.lastId,
      value: value,
      checked: false
    };

    axios.post('/data', record)
      .then((response) => {
        if (response.data.success) {
          const todoItems = me.state.todoItems;

          todoItems.push(record);

          me.setState({
            todoItems: todoItems
          });
        }
      })
      .catch((error) => {
        console.log(error);
        alert('Error occurred. See details in the developer console (F12).');
      });
  };

  changeStatus (id) {
    const me = this;
    const todoItems = this.state.todoItems;
    const index = todoItems.findIndex((item) => {
      return item._id === id;
    });

    if (index >= 0) {
      const record = todoItems[index];
      axios.put('/data', {
        _id: record._id,
        checked: !record.checked
      })
        .then((response) => {
          if (response.data.success) {
            todoItems[index].checked = !todoItems[index].checked;

            me.setState({
              todoItems: todoItems
            });
          }
        })
        .catch((error) => {
          console.log(error);
          alert('Error occurred. See details in the developer console (F12).');
        });
    }
  }

  removeRecord (id) {
    const me = this;
    const todoItems = this.state.todoItems;
    const index = todoItems.findIndex((item) => {
      return item._id === id;
    });

    if (index >= 0 ) {
      axios.delete('/data', {
        data: {
          _id: id
        }
      })
        .then((response) => {
          if (response.data.success) {
            todoItems.splice(index, 1);

            me.setState({
              todoItems: todoItems
            });
          }
        })
        .catch((error) => {
          console.log(error);
          alert('Error occurred. See details in the developer console (F12).');
        });


    }
  }

  render () {
    return (
      <div className={styles.mainContainer}>
        <Input
          addRecord={this.addRecord}
        />
        <List
          todoItems={this.state.todoItems}
          changeStatus={this.changeStatus}
          removeRecord={this.removeRecord}
        />
      </div>
    );
  }
}

export default App;