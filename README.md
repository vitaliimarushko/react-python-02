###Deploy
Execute from the project root folder:
```text
cd client/ && npm i && npm run build
cd ../server/
python -m venv venv
pip install Flask
python -m pip install pymongo
```
###Run:
Execute from the `server` folder:
```text
python server.py
```
###Open in browser
[http://localhost:5001](http://localhost:5001)