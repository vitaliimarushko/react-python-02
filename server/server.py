from flask import \
    Flask,\
    request,\
    json, \
    jsonify,\
    render_template

from db import Db


app = Flask(__name__,
            static_folder="../client/build",
            template_folder="../client"
            )


@app.route("/", methods=['GET'])
def index():
    return render_template("index.html")


@app.route("/data", methods=['GET'])
def get():
    return jsonify(
        success=True,
        data=list(Db.get_records(Db))
    )


@app.route("/data", methods=['POST'])
def insert():
    Db.insert_record(Db, json.loads(json.dumps(request.get_json())))

    return jsonify(
        success=True
    )


@app.route("/data", methods=['PUT'])
def change():
    Db.update_record(Db, json.loads(json.dumps(request.get_json())))

    return jsonify(
        success=True
    )


@app.route("/data", methods=['DELETE'])
def remove():
    Db.remove_record(Db, request.get_json()["_id"])

    return jsonify(
        success=True
    )


if __name__ == "__main__":
    app.run(
        debug=True,
        port=5001
    )
