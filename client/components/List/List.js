import React from 'react';

import Record from './../Record/Record.js';

class List extends React.Component {
  render () {
    const data = this.props.todoItems || [];

    return (
      <div>
        <ul>
          { data.map((item) => {
            return <Record
              key={item._id}
              record={item}
              changeStatus={this.props.changeStatus}
              removeRecord={this.props.removeRecord}
            />;
          }) }
        </ul>
      </div>
    );
  };
}

export default List;